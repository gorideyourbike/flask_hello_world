#!/home4/aejones/python27/.virtenv/flask_hello_world/bin/python

from flup.server.fcgi import WSGIServer
from flask_hello_world_app import app as application

if __name__ == '__main__':
    WSGIServer(application).run()
